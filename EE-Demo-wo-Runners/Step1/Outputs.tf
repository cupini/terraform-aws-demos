output "GitLab URL" {
  value = "${var.GL_Host_Name}${random_string.zone.result}.${var.GL_Domain}"
}

output "Public IP address of GitLabEE" {
  value = "${aws_instance.GitLabEE.public_ip}"
}

output "Private IP address of GitLabEE" {
  value = "${aws_instance.GitLabEE.private_ip}"
}

output "URL to grab Access Token" {
  value = "https://${var.GL_Host_Name}${random_string.zone.result}.${var.GL_Domain}/profile/personal_access_tokens"
}
