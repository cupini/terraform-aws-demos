

data "aws_ami" "OpenLDAP" {
  most_recent      = true

  filter {
    name   = "name"
    values = ["*KH-OpenLDAP*"]
  }

  filter {
    name = "tag:Button_Pusher"
    values =  ["*khair@gitlab.com*"]
  }
  #
  # name_regex = "^ami-\\d{3}"
  owners     =  ["self"]
}

resource "aws_instance" "OpenLDAP" {
  ami =  "${data.aws_ami.OpenLDAP.id}"
  instance_type = "${var.OpenLDAP_EC2_instance_type}"
  vpc_security_group_ids = [
    "${aws_security_group.SSH_Only.id}",
    "${aws_security_group.Allow_LDAP.id}",
    "${aws_security_group.Allow_ICMP_LDAP.id}"
    ]
  key_name = "${var.ssh_key}"
  tags {
      Name = "${var.OpenLDAP_friendly_name}"
    }


   connection {
   user = "ubuntu"
   private_key = "${file("${var.private_ssh_key_dir}")}"

   }
}
