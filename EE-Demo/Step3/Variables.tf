variable "gitlab_token" {
  default = "EycXhTk33o3wvRWAJr-v"
  description = "The Personal Access token with API access. THIS MUST BE CHANGED OR YOUR DEMO WILL NOT WORK!."
}

variable "gitlab_url" {
  default = "gitlab.263sjqw.gl-demo.io"
  description = "Full URL GitLab demo environment. THIS MUST BE CHANGED OR YOUR DEMO WILL NOT WORK!"
}

# Alternatively, you can also add the variables to the terraform apply command.
# The command line variables with supercede the above.
# Example:
# terraform apply -var 'gitlab_token=ExNM5pRVzzzP2wRs9jix' -var 'gitlab_url=gitlab.2i34v06.gl-demo.io'
