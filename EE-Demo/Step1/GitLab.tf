
data "aws_ami" "GitLab-EE" {
  most_recent      = true

  filter {
    name   = "name"
    values = ["*KH-GitLab-EE*"]
  }

  filter {
    name = "tag:Button_Pusher"
    values =  ["*khair@gitlab.com*"]
  }
  #
  # name_regex = "^ami-\\d{3}"
  owners     =  ["self"]
}



resource "aws_instance" "GitLabEE" {
  ami =  "${data.aws_ami.GitLab-EE.id}"
  instance_type = "${var.GitLabEE_EC2_instance_type}"
  vpc_security_group_ids = [
    "${aws_security_group.HTTP_HTTPS_SSH.id}",
    "${aws_security_group.Allow_ICMP_GL.id}"
    ]
  key_name = "${var.ssh_key}"
  tags {
      Name = "GitLab_EE-${random_string.zone.result}"
    }

    provisioner "remote-exec" {
      inline = [
        "echo ${var.GL_Host_Name}${random_string.zone.result}.${var.GL_Domain} > /tmp/fqdn.txt",
      ]
    }

    provisioner "file" {
      source = "scripts/reconfigure.sh",
      destination = "/tmp/reconfigure.sh"
    }

   provisioner "remote-exec" {
     inline = [
     "chmod +x /tmp/reconfigure.sh",
     "sudo /tmp/reconfigure.sh"
   ]
  }

  connection {
    user = "ubuntu"
    private_key = "${file("${var.private_ssh_key_dir}")}"
  }


  provisioner "local-exec" {
    command = "echo ${self.public_ip} > ../Step2/inventory"
  }



}
