``ansible-playbook -i inventory ansible-https.yml``

No changes are needed to run this script. However, you must have Ansible installed locally. 

Inventory file is automatically created in Step1 by Terraform.
