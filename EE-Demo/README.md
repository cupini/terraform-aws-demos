# GitLab EE Demo Builder

This project contains the Terraform plan, Ansible playbooks & BASH scripts needed to bring up a genuine working AWS environment.

Each "step" folder contains a purpose:
* Step1 - Deploy a base, non-SSL enabled GitLab EE into AWS.
* Step2 - Reconfigure the GitLab EE instance to use SSL via letsencrypt.
* Step3 - Add users, groups & projects to the GitLab EE using GitLab provider in Terraform.
* Step4 - License the server using an API call.
* Step5 - **To Do** Spin up & register two Docker based GitLab runner


## Getting Started

Currently, this project is locked down. Feel free to clone/mirror it & change it.

### Prerequisites

If you want to build in your local environment then you will need to install:

* [HashiCorp Terraform](https://www.terraform.io/downloads.html)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)


The scripts assume you have exported your AWS programatic credentials. The format for exporting is:

* ``export AWS_ACCESS_KEY_ID="yourkeyid"``
* ``export AWS_SECRET_ACCESS_KEY="yoursecretid"``

### TO ADD
