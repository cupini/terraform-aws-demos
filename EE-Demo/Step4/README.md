``./licensed.sh``

Runs locally & connects to the GitLab EE API via curl. 

Prompts include: 
* Demo URL to use
* API Access Token
* Location & Name of the License file to use.
